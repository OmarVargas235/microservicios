const bull = require('bull');

const redis = { host: '192.168.88.113', port: '6379' };

const opts =  { redis: { host: redis.host, port: redis.port } };

const queueCreate= bull("curso:create", opts);
const queueDelete = bull("curso:delete", opts);
const queueUpdate = bull("curso:update", opts);
const queueFindone = bull("curso:findOne", opts);
const queueView = bull("curso:view", opts);

async function Create({ age, color, name }) {

    try {

        const job = await queueCreate.add({ age, color, name });
    
        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function Delete({ id }) {

    try {

        const job = await queueDelete.add({ id });
    
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function FindOne({ id }) {

    try {

        const job = await queueFindone.add({ id });
    
        const { statusCode, data, message } = await job.finished();
        
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function Update({ name, age, color, id }) {

    try {

        const job = await queueUpdate.add({ name, age, color, id });
    
        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function View({}) {
   
    try {
        
        const job = await queueView.add({});
        
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function main() {

    // await Create({name: 'omar', edad: 22, color: 'green'});
    // await Delete({id: 10});
    // await Update({ name: "jaimes", age: 57, color: 'pink', id: 7 });
    await FindOne({ id: 16 });
    // await View({});
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
}