import { useEffect, useState, useMemo } from 'react';
import { io } from 'socket.io-client';


function App() {
    
    const [id, setId] = useState();
    const [data, setData] = useState([]);

    const socket = useMemo(() => io('http://localhost', {
        transports: ['websocket'],
        jsonp: false,
    }), []);

    useEffect(() => {

        setTimeout(() => setId(socket.id), 200);

        socket.on('res:microservice:view', ({ statusCode, data, message }) => {

            console.log('res:microservice:view', { statusCode, data, message });

            statusCode === 200 && setData(data);
        });

        socket.on('res:microservice:create', ({ statusCode, data, message }) => {

            console.log('res:microservice:create', { statusCode, data, message });
        });

        socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {

            console.log('res:microservice:findOne', { statusCode, data, message });
        });

        socket.on('res:microservice:update', ({ statusCode, data, message }) => {

            console.log('res:microservice:update', { statusCode, data, message });
        });

        socket.on('res:microservice:delete', ({ statusCode, data, message }) => {

            console.log('res:microservice:delete', { statusCode, data, message });
        });

        socket.emit('req:microservice:view', {});

    }, [socket]);

    
    return (
        <div>
            <p>{id ? `Estas en linea ${id}` : 'Fuera de linea'}</p>

            {
                data.map((data, index) => (
                    <p key={index}>Nombre: {data.name}; Edad: {data.age}; id: {data.id}</p>
                ))
            }
        </div>
    );
}

export default App;
