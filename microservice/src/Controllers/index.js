const { Model } = require('../Models');

async function Create({ name, age, color }) {

    try {

        const instance = await Model.create(
            { name, age, color },
            { fields: ['name', 'age', 'colors'], logging: false }
        );
        
        return { statusCode: 200, data: instance.toJSON() };
         
    } catch (err) {

        console.log({ step: 'controller Create', error: err.toString() });

        return { statusCode: 500, message: err.toString() };
    }
}

async function Delete({ where={} }) {

    try {

        await Model.destroy({ where, logging: false });
        
        return { statusCode: 200, data: "OK" };

    } catch (err) {

        console.log({ step: 'Delete Create', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

async function Update({ name, age, color, id }) {

    try {

        const instance = await Model.update(
            { name, age, color },
            { where: { id }, logging: false, returning: true },
        );

        console.log("instanceinstanceinstance", instance);
        console.log(instance[1][0]);
        
        return instance[1][0]
            ? { statusCode: 200, data: instance[1][0].toJSON() }
            : { statusCode: 200, data: null, message : "Usuario no existe" };

    } catch (err) {

        console.log({ step: 'controller Update', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

async function FindOne({ where={} }) {
    
    try {

        const instance = await Model.findOne({ where, logging: false });
        
        return instance
            ? { statusCode: 200, data: instance.toJSON() }
            : { statusCode: 400, message: 'No existe el usuario' };

    } catch (err) {

        console.log({ step: 'controller FindOne', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

async function View({ where={} }) {

    try {

        const instances = await Model.findAll({ where, logging: false });
        
        return { statusCode: 200, data: instances };

    } catch (err) {

        console.log({ step: 'controller View', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
}