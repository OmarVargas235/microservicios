const Controllers = require('../Controllers/');
const { InternalError } = require('../settings');

async function Create({ age, color, name }) {
    
    try {

        const { statusCode, data, message } = await Controllers.Create({ age, color, name });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service Create" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function Delete({ id }) {
    
    try {

        const findOne = await Controllers.FindOne({ whuere: { id } });

        if (findOne.statusCode !== 200) {

            const response = {
                400: { statusCode: 400, message: "No existe el usuario a eliminar", },
                500: { statusCode: 500, message: InternalError },
            }

            return response[findOne.statusCode];
        }

        const del = await Controllers.Delete({ where: { id } });

        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data };

        return { statusCode: 400, data: InternalError };
    
    } catch(err) {

        console.log({step:"service Delete" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function Update({ age, color, name, id }) {
    
    try {

        const { statusCode, data, message } = await Controllers.Update({ age, color, name, id });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service Update" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function FindOne({ id }) {
    
    try {

        const { statusCode, data, message } = await Controllers.FindOne({ where: { id } });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service FindOne" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function View() {
    
    try {

        const { statusCode, data, message } = await Controllers.View({});

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service View" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
}