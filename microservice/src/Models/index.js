const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define('curso', {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    color: { type: DataTypes.STRING },
});

const SyncDB = async () => {
    
    try {

        console.log("Vamos a inicializar la base de datos");

        await Model.sync({ logging: false });

        console.log("Base de datos inicializada");

        return { statusCode: 200, data: 'ok' }

    } catch (err) {

        console.log(err);

        return { statusCode: 500, message: err.toString() }
    }
}

module.exports = { SyncDB, Model };